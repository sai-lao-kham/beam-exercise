# To understand where all the scooters currently are

- Backend
    - `aws iot` ( mqtt server )
    - all the scooters have to connect, publish their coordinates every seconds

- Frontend
    - we can use react, angular for web and react-native, ionic for mobile development, anything ok.
    - In order to connect, those clients app needs credentials to connect `aws iot`. For this, we can use `aws cognito` to get credential and for security point of view, it prevents from attacker to get credential keys in the browser/app
    - Then, clients have to subscribe to the `aws iot` via mqtt js and aws-sdk for client side
    - And clients will get real time information/movement from all the scooters like where they are, where they go


# Backend

- `aws iot` ( mqtt server )
- all the scooters have to connect, publish their coordinates every seconds
- we can save real-time data from all scooters into `dynamodb` as a timeseries using rule engine ( a nice feature what `dyanmodb` has)
- if we have some persistent data like user information, we can store into `aws rds`
- if we want to calculate reports or something based on specific scooter, we can query from the `dynamodb` and invoke the `lambda` function to do the task  as `lambda` is the most popular event-driven stateless function.
- for security, we can put out `dynamodb` in private subnet and no one can access to our data
- To implement business logic, we can dockerize our backend service and deploy into `aws fargate` and it's easy to scale our app.

# fontend ( app, website )

- we can use react, angular for web and react-native, ionic for mobile development or anything ok.
- In order to connect, those clients app needs credentials to connect `aws iot`. For this, we can use `aws cognito` to get credential and for security point of view, it prevents from attacker to get credential keys in the browser/app
- Then, clients have to subscribe to the `aws iot` via mqtt js and aws-sdk for client side
- And clients will get real time information/movement from all the scooters like where they are, where they go

---

## Definitions

### AWS IOT

The `AWS IoT` service is an MQTT message gateway that can send and receive MQTT messages to and from devices or things. All `AWS IoT` services are build AWS Serverless, means all AWS customers get full benefit of the elasticity of the AWS cloud to build their enterprise applications and they don’t have to worry about scalability or utilisation. Customers only pay for what they use.This means no matter what business we are in, `AWS IoT` can help us extract value from our connected devices.

### Dynamodb

`DynamoDB` is a managed NoSQL database provided by AWS, and it is a highly scalable and reliable database. We can scale from 10 to 1000 transactions per second (tps) in couple of seconds. Since it is a managed service, we don’t need to worry about the underlying hardware, servers or operating system. Data stored in the `DynamoDB` is redundantly copied across multiple Availability Zones so by default it provides protection for data loss due to underlying hardware failures.

### RDS

Amazon Relational Database Service (Amazon RDS) is a web service that makes it easier to set up, operate, and scale a relational database in the cloud. It provides cost-efficient, resizable capacity for an industry-standard relational database and manages common database administration tasks.

### Cognito

Amazon Cognito allows to add registration and authentication to Web and mobile applications. It allows users to authenticate either through a fully managed user pool or through an external identity provider (IdP). It can also provide temporary security credentials to access AWS resources.

### AWS Fargate

AWS Fargate is a way to avoid messing around with EC2 instances, running Docker deamons, managing versions and Devops in general. Basically, `Fargate` runs a Docker container and manages the infrastructure — so we don’t have to.



