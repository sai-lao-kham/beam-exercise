const {generateRandomPoint} = require('./../utils/utils')

const Scooter = {
    getRamdomScooters(req, res){
        // center of singapore https://www.geodatos.net/en/coordinates/singapore
        const center = {
            lat: 1.352083,
            lng: 103.8198395
        }

        const radius = 5000; // metres
        const count = Math.floor(30 + Math.random() * 100)

        const points = []

        for(let i = 0; i < count; i++){
            points.push(generateRandomPoint(center, radius, i))
        }

        res.json({
            scooters: points,
            count: count
        })
    }
}

module.exports = {
    Scooter
}