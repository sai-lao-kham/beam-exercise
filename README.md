# Find Nearest Scooters

First of all, thank you so much for the opportunity to take the coding test.

This project help you find the nearest scooters within the specific radius and you can also limit the amount of scooters to search. I use two icons to specify the human and nearest scooters. All the scooters are generated randomly by the backend and whenever you refresh the browser, the scooters are updated.


## Built With

- HTML, CSS, Js
- NodeJs

## Run it locally

1. Install nodejs

2. Clone this repo

```
npm install

npm start
```

3. Continue, then open a local browser and go to `http://localhost:8000`

### Demo

ScreenShot:

<p align="center">
  <img src = "https://i.imgur.com/poMdzSq.png" width=500>
</p>

## What I Learned

- K closest points to the Origin
> The distance between two points on a plane is the [Euclidean distance](https://en.wikipedia.org/wiki/Euclidean_distance).

```
√{(x2-x1)2 + (y2-y1)2}
```


## Acknowledgments

- Special thanks to [Beam](https://www.ridebeam.com/) family for such a nice coding test
