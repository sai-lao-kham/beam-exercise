const express = require('express');
const path = require('path');
const {Scooter} = require('./controller/scooter')

let app = express();
app.use('/', express.static(path.join(__dirname, 'public')))

app.get('/scooter', Scooter.getRamdomScooters)

const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`Server is listening on port: ${port}`));