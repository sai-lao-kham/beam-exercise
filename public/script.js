(function(){
	let map,
	searchArea,
	searchAreaMarker,
	// center of singapore
	startLat = 1.352083,
	startLng = 103.8198395,

	startLatLng = null,
	randomMarkers = null,
	radius = null, // metres
	button = null,
	iconBase = './icons/',
	icons = {
		scooter: {
			icon: iconBase + 'blue.png'
		},
		human: {
			icon: iconBase + 'red.png'
		}
	},
	dictionaryPoints = {};

function find() {
	// collect no.of scooters and radius which user set up
	let scooter = Number(document.getElementById('scooter').value);
	radius = document.getElementById('radius').value;

	// remove the old circle
	searchArea.setMap(null);

	// create a new circle with a new position
	searchArea = new google.maps.Circle({
		strokeColor: '#FF0000',
		strokeOpacity: 0.5,
		strokeWeight: 2,
		fillColor: '#FF0000',
		fillOpacity: 0.2,
		map: map,
		center: startLatLng,
		radius: Number(radius)
	});

	for (let i = 0; i < randomMarkers.length; i++) {
		if (dictionaryPoints[randomMarkers[i].latLng.lat().toString()]) {
			// removing blue icon
			randomMarkers[i].marker.setMap(null);

			randomMarkers[i].marker = new google.maps.Marker({
				position: randomMarkers[i].latLng,
				map: map,
				title: randomMarkers[i].title
			});
		}
		// calculate distance between current position and each scooter
		randomMarkers[i].dist =
			(randomMarkers[i].latLng.lat() - startLat) * (randomMarkers[i].latLng.lat() - startLat) +
			(randomMarkers[i].latLng.lng() - startLng) * (randomMarkers[i].latLng.lng() - startLng);
	}

	// sort the markers in ascending order based on the distance
	const result = randomMarkers.sort((a, b) => a.dist - b.dist);
	// filter the array based on no.of scooter
	const filterResult = result.slice(0, scooter);

	// find markers in area
	for (let i = 0; i < filterResult.length; i++) {
		// checking filter Result are whether inside the circle or not
		if (
			google.maps.geometry.spherical.computeDistanceBetween(
				filterResult[i].marker.getPosition(),
				searchArea.getCenter()
			) <= searchArea.getRadius()
		) {
			// remove existing marker
			{
				filterResult[i].marker && filterResult[i].marker.setMap(null);
			}

			// in order to delete marker later, use dictionary approach
			dictionaryPoints[
				filterResult[i].marker
					.getPosition()
					.lat()
					.toString()
			] =
				filterResult[i].title;

			// set up a new marker with new icon
			filterResult[i].marker = new google.maps.Marker({
				position: new google.maps.LatLng(
					filterResult[i].marker.getPosition().lat(),
					filterResult[i].marker.getPosition().lng()
				),
				map: map,
				icon: icons.scooter.icon,
				title: filterResult[i].title
			});
		}
	}
}

async function init() {
	startLatLng = new google.maps.LatLng(startLat, startLng);
	radius = Number(document.getElementById('radius').value);

	button = document.querySelector('button');
	button.addEventListener('click', find)

	// initialize map
	map = new google.maps.Map(document.getElementById('map-canvas'), {
		center: startLatLng,
		zoom: 12
	});

	// initialize radius with default radius
	searchArea = new google.maps.Circle({
		strokeColor: '#FF0000',
		strokeOpacity: 0.5,
		strokeWeight: 2,
		fillColor: '#FF0000',
		fillOpacity: 0.2,
		map: map,
		center: startLatLng,
		radius: radius
	});

	// initialize human location with default position
	searchAreaMarker = new google.maps.Marker({
		position: startLatLng,
		map: map,
		icon: icons.human.icon,
		draggable: true,
		animation: google.maps.Animation.DROP,
		title: 'searchAreaMarker'
	});

	// initialize current position to track
	const currentPosition = document.getElementById('current');

	// fetch a list of scooters randomly
	const {scooters} = await fetch('http://localhost:8000/scooter')
	.then(response => response.json())
	.then(json => json)
	.catch(e => console.log('e is ', e))

	// transform a list of scooters into an array of google map postion object
	randomMarkers = scooters.map(scooter => ({
		title: 'Marker ' + scooter.title,
		latLng: new google.maps.LatLng(scooter.lat, scooter.lng)
	}))

	// put markers on google map
	for (let i = 0; i < randomMarkers.length; i++) {
		randomMarkers[i].marker = new google.maps.Marker({
			position: randomMarkers[i].latLng,
			map: map,
			title: randomMarkers[i].title
		});
	}

	// add eventlistener to track our human marker's position
	google.maps.event.addListener(searchAreaMarker, 'dragend', function(e) {
		startLatLng = e.latLng;
		startLat = startLatLng.lat();
		startLng = startLatLng.lng();

		currentPosition.innerHTML = startLatLng.lat() + ',' + startLatLng.lng();

		searchArea.setOptions({
			center: startLatLng
		});

		map.panTo(searchAreaMarker.getPosition());
	});
}

google.maps.event.addDomListener(window, 'load', init);
})()